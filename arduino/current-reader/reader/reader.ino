#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))

const int max471AnalogIn = A0;
const float adc_factor = 5.0 / 1024.0;

const int NUM_MEAS = 300;
const int NUM_STEPS = 43;

float amps[NUM_MEAS];

int raw = 0;
int step = 0;  

void setup() {
  // put your setup code here, to run once:
  pinMode(max471AnalogIn, INPUT);
  // ADC prescaler=16(100) => sampling rate to 76.8KHz
  sbi(ADCSRA, ADPS2);
  sbi(ADCSRA, ADPS1);
  sbi(ADCSRA, ADPS0);
 
  Serial.begin(115200);
}

void loop() {
  int i = 0;
  while (i < NUM_MEAS)
  {
    raw = analogRead(max471AnalogIn); 
    amps[i] = raw * adc_factor; // scale the ADC       
    i++;
    //delay(10);
  }
  for (i = 0; i < NUM_MEAS; i++)
  {
    Serial.print(amps[i], 3); //3 digits after decimal point
    Serial.print(' ');
  }
  Serial.println("");
//  step++;
//  if (step > NUM_STEPS)
//  {
//    while(1);
//  }
}
