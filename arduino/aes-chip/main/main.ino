#include <AESLib.h>
#include "LowPower.h"

#include "data.h"

//#define DEBUG 1
#define NUM_STEPS 100

String plain;
byte *key = (unsigned char*)"Universitate2019";//"0123456789010123";
                            
unsigned long long int init_vec = 36753562;


const int plainLength = 16;
int step = 0;

void r_100_aes()
{     
  for(int j=0; j<100; j++)
  {
    aes128_enc_single(key, data[step]);
    aes128_dec_single(key, data[step]);
  }
}

// the setup function runs once when you press reset or power the board
void setup() {
#ifdef DEBUG
   Serial.begin(115200); // opens serial port, sets data rate to 9600 bps
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
#else
  // enable clock speed changes
  CLKPR = 0x80;
  // set clock divider to to 256
  CLKPR = 0x08;
  // so clock speed = 16MHz / 256 = 62.5 kHz
#endif
}

// the loop function runs over and over again forever
void loop(){
  noInterrupts();
  for (step = 0; step < NUM_STEPS; step++)
  {
#ifdef DEBUG
    Serial.print("plain: "); 
    for (int i=0;i < plainLength; i++)
    {
      Serial.print((char)data[step][i]);
      Serial.print(' ');
    }
    Serial.print('\n');
    aes128_enc_single(key, data[step]);
#else
    // encryption
    //aes128_enc_single(key, data[step]);
    r_100_aes();
#endif

#ifdef DEBUG
    Serial.print("encrypted: ");
    for (int i=0;i < plainLength; i++)
    {
      Serial.print(data[step][i], HEX);
      Serial.print(' ');
    }
    Serial.print('\n');
#endif

#ifdef DEBUG  
    // decryption
    aes128_dec_single(key, data[step]);
  
    Serial.print("decrypted: ");
    for (int i = 0; i < plainLength; i++)
    {
      Serial.print((char)data[step][i]);
      Serial.print(' ');
    }
    Serial.print('\n');
  
    digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(100);                       // wait for a second
  
    digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
    delay(100);                       // wait for a second
#else
    //delay(50);
    LowPower.powerDown(SLEEP_2S, ADC_OFF, BOD_OFF);
#endif
  }
  while(1);
}
