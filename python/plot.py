import matplotlib.pyplot as plt
import numpy as np
import sys

if len(sys.argv) > 1:
  idx = int(sys.argv[1])
else:
  idx = 0;

# load traces from txt file with timestamps
with open('traces.txt') as f:
    content = f.readlines()
content = [x.strip() for x in content] 

amp_traces = np.array(len(content))
arr_of_arr = list()
for line in content:
  elems = line.split(" ")
  elems.pop(0)
  elems.pop(0)
  arr_of_arr.append(np.array(elems))
amp_traces = np.asarray(arr_of_arr).astype(np.float)
 
do_plot = True
 
if do_plot:
    plt.plot(amp_traces[idx])
    plt.show()
